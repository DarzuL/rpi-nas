#!/bin/sh

###
# feel free to modify options to better fit to your use case
# 
# here's an example of variable:
# DIRECTORY_TO_SHARE="/home/pi/movies"
# IP_MASK="192.168.1.0/24"
###

DIRECTORY_TO_SHARE=""
IP_MASK=""

# sudo apt-get purge rpcbind portmap nfs-common -y
sudo apt-get install rpcbind nfs-common nfs-kernel-server -y
echo "$DIRECTORY_TO_SHARE $IP_MASK(rw,async,no_root_squash,subtree_check)" | sudo tee /etc/exports

# credit [https://www.raspberrypi.org/forums/viewtopic.php?f=28&t=149002&p=980161]
# Original [https://lists.debian.org/debian-devel/2014/05/msg00618.html]

cat <<EOF | sudo tee -a /etc/systemd/system/nfs-common.services
[Unit]
Description=NFS Common daemons
Wants=remote-fs-pre.target
DefaultDependencies=no

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/etc/init.d/nfs-common start
ExecStop=/etc/init.d/nfs-common stop

[Install]
WantedBy=sysinit.target
EOF

cat <<EOF | sudo tee -a /etc/systemd/system/rpcbind.service
[Unit]
Description=RPC bind portmap service
After=systemd-tmpfiles-setup.service
Wants=remote-fs-pre.target
Before=remote-fs-pre.target
DefaultDependencies=no

[Service]
ExecStart=/sbin/rpcbind -f -w
KillMode=process
Restart=on-failure

[Install]
WantedBy=sysinit.target
Alias=portmap
EOF

sudo systemctl enable nfs-common
sudo systemctl enable rpcbind

sudo systemctl reload nfs-common.service
sudo systemctl reload nfs-kernel-server.service
