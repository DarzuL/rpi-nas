
case "$1" in
  backup)
    if [ -z "$2" ]
      then
        echo "backup command needs a directory as a parameter"
        exit 1
    fi

    mkdir -p $2

    # transmission
	cp /etc/transmission-daemon/settings.json "$2/transmission-settings.json" || exit 1

	# couchpotato
	sudo service stop couchpotato stop
	sudo rsync -a --progress /opt/CouchPotato/settings.conf "$2/couchpotato/settings.conf"
	sudo rsync -a --progress /opt/CouchPotato/database/ "$2/couchpotato/database/"

	echo "Sonarr: https://github.com/Sonarr/Sonarr/wiki/Backup-and-Restore"
	
  ;;
  restore)
    if [ -z "$2" ]
      then
        echo "restore command needs a directory as a parameter"
        exit 1
    fi
	
	cp "$2/transmission-settings.json" /etc/transmission-daemon/settings.json || exit 1
	
	# couchpotato
	sudo service stop couchpotato stop
	sudo mv ~/.couchpotato/database/ ~/.couchpotato/database.backup
	sudo rsync -a --progress "$2/couchpotato/settings.conf" ~/.couchpotato/settings.conf
	sudo rsync -a --progress "$2/couchpotato/database/" ~/.couchpotato/database/
	
	
    # sonarr
	echo "Sonarr: https://github.com/Sonarr/Sonarr/wiki/Backup-and-Restore"
    rm -f ~/.config/NzbDrone/config.xml ~/.config/NzbDrone/nzbdrone.db*
    # Todo: extract backup to ~/.config/NzbDrone/
	
  ;;
  *)
    echo "Usage backup.sh {backup <path>|restore <url>}"
    exit 1
  ;;
esac


# transmission
# config file: /etc/transmission-daemon/settings.json
# reload: sudo /etc/init.d/transmission-daemon reload

# nfs
# config file: /etc/exports
# reload: sudo service nfs-kernel-server restart