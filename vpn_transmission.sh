#!/bin/bash

VPN_USERNAME=""
VPN_PASSWORD=""
VPN_CONFIG_URL=""

sudo apt-get install openvpn resolvconf -y

mkdir $HOME/vpn
cd $HOME/vpn

wget $VPN_CONFIG_URL -O config.ovpn
echo "$VPN_USERNAME
$VPN_PASSWORD" | tee credentials.txt
sudo chmod 600 credentials.txt
replaceText "auth-user-pass" "auth-user-pass $HOME/vpn/credentials.txt" $HOME/vpn/config.ovpn

echo "#!/bin/bash

mkdir -p /dev/net
mknod /dev/net/tun c 10 200
chmod 600 /dev/net/tun
cat /dev/net/tun

openvpn --client --config $HOME/vpn/config.ovpn --up $HOME/vpn/up.sh --script-security 2" | tee vpn.sh

echo '#!/bin/sh

/etc/init.d/transmission-daemon stop
sed "s/\"bind-address-ipv4\": .*$/\"bind-address-ipv4\": $4,/g" /etc/transmission-daemon/settings.json
/etc/init.d/transmission-daemon start' | tee up.sh
chmod +x up.sh
chmod +x vpn.sh

echo '#!/bin/sh
### BEGIN INIT INFO
# Provides:          vpn_transmission
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       VPN connection for Transmission
### END INIT INFO

SCRIPT='$HOME'/vpn/vpn.sh
RUNAS=root

PIDFILE=/var/run/vpn_transmission.pid
LOGFILE=/var/log/vpn_transmission.log

start() {
  if [ -f /var/run/$PIDNAME ] && kill -0 $(cat /var/run/$PIDNAME); then
    echo "Service already running" >&2
    return 1
  fi
  echo "Starting service…" >&2
  local CMD="$SCRIPT &> \"$LOGFILE\" & echo \$!"
  su -c "$CMD" $RUNAS > "$PIDFILE"
  echo "Service started" >&2
}

stop() {
  if [ ! -f "$PIDFILE" ] || ! kill -0 $(cat "$PIDFILE"); then
    echo "Service not running" >&2
    return 1
  fi
  echo "Stopping service…" >&2
  kill -15 $(cat "$PIDFILE") && rm -f "$PIDFILE"
  echo "Service stopped" >&2
}

uninstall() {
  echo -n "Are you really sure you want to uninstall this service? That cannot be undone. [yes|No] "
  local SURE
  read SURE
  if [ "$SURE" = "yes" ]; then
    stop
    rm -f "$PIDFILE"
    echo "Notice: log file is not be removed: $LOGFILE" >&2
    update-rc.d -f <NAME> remove
    rm -fv "$0"
  fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  uninstall)
    uninstall
    ;;
  restart)
    stop
    start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|uninstall}"
esac' | sudo tee /etc/init.d/vpn_transmission
sudo chmod +x /etc/init.d/vpn_transmission
sudo update-rc.d vpn_transmission defaults
