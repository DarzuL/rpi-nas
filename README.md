# rpi-nas

Scripts to install a media center raspberrypi


```sh
sudo apt-get install git-core
git clone https://bitbucket.org/DarzuL/rpi-nas.git
cd rpi-nas
sudo bash setup.sh
```

## Ports

- *kodi*: 8080
- *couchpotato*: 5050
- *transmission*: 9091
- *sabnzbd*: 8081
- *sonarr*: 8989

## Auto mount disks
To auto mount your disks at boot you should edit `/etc/fstab`:
```text
#<file system>       <dir>       <type>         <options>     <dump> <pass>
/dev/sdXY       /home/pi/toto    ntfs-3g        defaults        1      1
UUID=XXX-XXX    /home/pi/titi    ext4           defaults        1      1
```
If you have a ntfs partition, don't forget to install `ntfs-3g`:
```sh
sudo apt-get install ntfs-3g
```