##
# Sabnzbd install script for Rpi
##

SABNZBD_HOST=0.0.0.0
SABNZBD_PORT=8081

sudo apt-get install python python-gdbm python-cheetah python-openssl par2 unzip -y

echo "deb http://ppa.launchpad.net/jcfp/ppa/ubuntu precise main" | sudo tee -a /etc/apt/sources.list
sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net:11371 --recv-keys 0x98703123E0F52B2BE16D586EF13930B14BB9F05F

echo "deb-src http://archive.raspbian.org/raspbian/ jessie main contrib non-free rpi" | sudo tee -a /etc/apt/sources.list
sudo apt-get update
cd $HOME && mkdir unrar && cd unrar
sudo apt-get build-dep unrar-nonfree -y
sudo apt-get source -b unrar-nonfree
sudo dpkg -i unrar_*_armhf.deb
cd $HOME
sudo rm -rf unrar

# install yEnc
sudo pip uninstall sabyenc

sudo apt-get update
sudo apt-get install sabnzbdplus -y

replaceText "USER=.*$" "USER=$USER" /etc/default/sabnzbdplus
replaceText "HOST=.*$" "HOST=$SABNZBD_HOST" /etc/default/sabnzbdplus
replaceText "PORT=.*$" "PORT=$SABNZBD_PORT" /etc/default/sabnzbdplus

sudo update-rc.d sabnzbdplus defaults
