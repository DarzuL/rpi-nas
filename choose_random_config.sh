#!/bin/bash

BASE_DIR="/home/osmc/vpn/"
CONFIG="config.ovpn"
CONFIG_DIR="configs/"
CONFIG_FILTER="*.ovpn"
CREDENTIALS="credentials.txt"

cd $BASE_DIR

config_count=$(ls $CONFIG_DIR$CONFIG_FILTER | wc -l)
new_config_index=$(( ( RANDOM % $config_count )  + 1 ))
new_config=$(ls $CONFIG_DIR$CONFIG_FILTER | sed -n -e "${new_config_index}{p;q}")

echo "VPN will be: $new_config"
cp $new_config $CONFIG

sed -i "s/^auth-user-pass$/auth-user-pass $CREDENTIALS/" $CONFIG