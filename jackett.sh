#
# Jackett install script
#

apt-get install libcurl4-openssl-dev bzip2 -y
jackettver=$(wget -q https://github.com/Jackett/Jackett/releases/latest -O - | grep -E \/tag\/ | awk -F "[><]" '{print $3}')
wget -q https://github.com/Jackett/Jackett/releases/download/$jackettver/Jackett.Binaries.Mono.tar.gz

tar -xvf Jackett.Binaries.Mono.tar.gz
mkdir /opt/jackett
mv Jackett/* /opt/jackett
chown -R $USER:$USER /opt/jackett

echo "[Unit]
Description=Jackett Daemon
After=network.target

[Service]
User=$USER
Restart=always
RestartSec=5
Type=simple
ExecStart=/usr/bin/mono --debug /opt/jackett/JackettConsole.exe
TimeoutStopSec=20

[Install]
WantedBy=multi-user.target" | tee /etc/systemd/system/jackett.service

systemctl enable jackett
service jackett start