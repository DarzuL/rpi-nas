#!/bin/sh

sudo apt-get update
sudo apt-get install -y python3-systemd
cd /tmp
wget http://ftp.fr.debian.org/debian/pool/main/f/fail2ban/fail2ban_0.9.7-1_all.deb
sudo dpkg -i fail2ban_0.9.7-1_all.deb
rm fail2ban_0.9.7-1_all.deb

echo "[DEFAULT]
backend = systemd
" | sudo tee /etc/fail2ban/jail.d/customisation.local

sudo systemctl enable fail2ban
sudo systemctl start fail2ban