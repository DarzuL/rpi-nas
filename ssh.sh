##
# SSH public key install script for Rpi
##

SSH_PUBLIC_KEYS=''

display_title "Add authorized ssh keys"
mkdir $HOME/.ssh
echo "$SSH_PUBLIC_KEYS" | tee $HOME/.ssh/authorized_keys


display_title "Enable SSH public key auth"
sudo sed -i 's/^ChallengeResponseAuthentication .*$//g' /etc/ssh/sshd_config
sudo sed -i 's/^PasswordAuthentication .*$//g' /etc/ssh/sshd_config
sudo sed -i 's/^UsePAM .*$//g' /etc/ssh/sshd_config
  echo '
# Authorize to login only with ssh key
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no' | sudo tee --append /etc/ssh/sshd_config
sudo /etc/init.d/ssh reload
