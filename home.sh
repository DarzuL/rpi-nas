#!/bin/sh

echo "[Service]
ExecStart=/usr/bin/node $HOME/rpi-nas/home/lib-node6/index.js
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=http-home
User=root
Environment=NODE_ENV=production
Environment=PORT=80

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/http-home.service

cd $HOME/rpi-nas/home/ && npm install && npm cache clean
cd -

sudo systemctl enable http-home.service
sudo systemctl start http-home.service
