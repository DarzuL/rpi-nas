##
# Transmission install script for Rpi
##

DOWNLOAD_DIR=""

sudo apt-get install -y transmission-daemon
sudo /etc/init.d/transmission-daemon stop
replaceText "\"rpc-whitelist-enabled\": true" "\"rpc-whitelist-enabled\": false" /etc/transmission-daemon/settings.json
replaceText "\"rpc-authentication-required\": true" "\"rpc-authentication-required\": false" /etc/transmission-daemon/settings.json
replaceText "\"download-dir\": .*$" "\"download-dir\": \"${DOWNLOAD_DIR}\"," /etc/transmission-daemon/settings.json
# 18 = 022 (644 for files and 755 for directories), 2 = 002 (664 for files and 775 for directories)
replaceText "\"umask\": 18" "\"umask\": 2" /etc/transmission-daemon/settings.json
sudo /etc/init.d/transmission-daemon reload
