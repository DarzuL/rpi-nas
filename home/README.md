# server-rpi-nas [![NPM version][npm-image]][npm-url]



[![Dependency Status][daviddm-image]][daviddm-url]


## Install

```sh
npm install --save server-rpi-nas
```

## Usage

```js
import serverRpiNas from 'server-rpi-nas';

console.log(serverRpiNas);
```

[npm-image]: https://img.shields.io/npm/v/server-rpi-nas.svg?style=flat-square
[npm-url]: https://npmjs.org/package/server-rpi-nas
[daviddm-image]: https://david-dm.org//server-rpi-nas.svg?style=flat-square
[daviddm-url]: https://david-dm.org//server-rpi-nas
