'use strict';

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _koaStatic = require('koa-static');

var _koaStatic2 = _interopRequireDefault(_koaStatic);

var _alpReactRedux = require('alp-react-redux');

var _alpReactRedux2 = _interopRequireDefault(_alpReactRedux);

var _HtmlComponent = require('./HtmlComponent');

var _HtmlComponent2 = _interopRequireDefault(_HtmlComponent);

var _HomeViewComponent = require('./HomeViewComponent');

var _HomeViewComponent2 = _interopRequireDefault(_HomeViewComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = new _koa2.default();
app.context.computeInitialContextForBrowser = function () {};
(0, _alpReactRedux2.default)(_HtmlComponent2.default)(app);

app.use((0, _koaStatic2.default)(`${ __dirname }/../public/`));

app.use(ctx => {
  ctx.render({ View: _HomeViewComponent2.default }, { host: `${ ctx.protocol }://${ ctx.hostname }` });
});

app.listen(process.env.PORT || 3000);
//# sourceMappingURL=index.js.map