"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Home;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Home(_ref) {
  let host = _ref.host;

  return _react2.default.createElement(
    "ul",
    { className: "list links" },
    _react2.default.createElement(
      "li",
      null,
      _react2.default.createElement(
        "a",
        { href: `${ host }:8080` },
        "Kodi"
      )
    ),
    _react2.default.createElement(
      "li",
      null,
      _react2.default.createElement(
        "a",
        { href: `${ host }:5050` },
        "Couchpotato"
      )
    ),
    _react2.default.createElement(
      "li",
      null,
      _react2.default.createElement(
        "a",
        { href: `${ host }:9091` },
        "Transmission"
      )
    ),
    _react2.default.createElement(
      "li",
      null,
      _react2.default.createElement(
        "a",
        { href: `${ host }:8081` },
        "Sabnzbd"
      )
    ),
    _react2.default.createElement(
      "li",
      null,
      _react2.default.createElement(
        "a",
        { href: `${ host }:8989` },
        "Sonarr"
      )
    )
  );
}
//# sourceMappingURL=HomeViewComponent.js.map