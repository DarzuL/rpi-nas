export default function Html(props) {
  return (
    <html>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <title>{props.title}</title>
        <meta name="description" content={props.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100,500italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="/simple-blue.css" />
        <link rel="stylesheet" href="/index.css" />
      </head>
      <body dangerouslySetInnerHTML={{ __html: props.body }} />
    </html>
  );
}
