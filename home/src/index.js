import Koa from 'koa';
import serve from 'koa-static';
import reactredux from 'alp-react-redux';
import Html from './HtmlComponent';
import HomeView from './HomeViewComponent';

const app = new Koa();
app.context.computeInitialContextForBrowser = function () {};
reactredux(Html)(app);

app.use(serve(`${__dirname}/../public/`));

app.use((ctx) => {
  ctx.render({ View: HomeView }, { host: `${ctx.protocol}://${ctx.hostname}`});
});

app.listen(process.env.PORT || 3000);
