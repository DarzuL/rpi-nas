export default function Home({ host }) {
  return (
    <ul className="list links">
      <li><a href={`${host}:8080`}>Kodi</a></li>
      <li><a href={`${host}:5050`}>Couchpotato</a></li>
      <li><a href={`${host}:9091`}>Transmission</a></li>
      <li><a href={`${host}:8081`}>Sabnzbd</a></li>
      <li><a href={`${host}:8989`}>Sonarr</a></li>
    </ul>
  );
}
