#!/bin/sh

MUMBLE_PORT=""
MUMBLE_SERVER_PASSWORD=""

sudo apt-get install mumble-server -y
sudo dpkg-reconfigure mumble-server

replaceText "^port=.*$" "port=$MUMBLE_PORT" /etc/mumble-server.ini
replaceText "^serverpassword=.*$" "serverpassword=$MUMBLE_SERVER_PASSWORD" /etc/mumble-server.ini

sudo service mumble-server restart
