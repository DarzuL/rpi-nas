##
# Couchpotato install script for Rpi
##

sudo apt-get update
sudo apt-get install git-core libffi-dev libssl-dev zlib1g-dev libxslt1-dev libxml2-dev python python-pip python-dev build-essential -y

sudo pip install lxml cryptography pyopenssl

sudo chown $USER:$USER /opt
git clone http://github.com/RuudBurger/CouchPotatoServer /opt/CouchPotato

python /opt/CouchPotato/CouchPotato.py --daemon

echo "CP_HOME=/opt/CouchPotato
CP_USER=$USER
CP_PIDFILE=/home/pi/.couchpotato.pid
CP_DATA=/opt/CouchPotato
CP_OPTS=--daemon" | sudo tee /etc/default/couchpotato

sudo cp /opt/CouchPotato/init/ubuntu /etc/init.d/couchpotato
sudo chmod +x /etc/init.d/couchpotato
sudo update-rc.d couchpotato defaults
