displayTitle(){
	echo "\033[33m> \033[1m" $1 "\033[0m"
}
export -f displayTitle

yesOrNo() {
	local message
	local default
	if [ "$2" = "y" ]; then message="$1 [Y/n]"; default='yes';
	elif [ "$2" = "n" ]; then message="$1 [y/N]"; default='no';
	else message="$1 [y/n]"; default=false;
	fi

	local yesNoAnswer=''
	while [ -n $yesNoAnswer ]; do
		read -p "$message " yesNoAnswer
		case $yesNoAnswer in
			[yY]|[yY][eE][sS])
				echo 'yes'
				exit
			;;
			[nN]|[nN][oO])
				echo 'no'
				exit
			;;
			'')
				if $default; then
					echo "$default"
					exit
				fi
			;;
		esac
	done
}
export -f yesOrNo

next() {
    sh boot.sh next $1
}
export -f next

escapeVariable() {
    echo "$(echo "$1" | sed -e 's/[\/()&]/\\&/g')"
}
export -f escapeVariable

replaceText() {
    var1=$(escapeVariable "$1")
    var2=$(escapeVariable "$2")
    sudo sed -i "s/$var1/$var2/g" $3
}
export -f replaceText

checkRoot() {
	if [ $USER != 'root' ]; then
		echo "You are not root, please execute this script with sudo."
		exit 1
	fi
}
export -f checkRoot
