#!/bin/sh

DIRECTORY_TO_SHARE=""

sudo apt-get install samba samba-common-bin -y
sudo chown -R $USER:$USER $DIRECTORY_TO_SHARE

echo "
[nas]
  path = $DIRECTORY_TO_SHARE      
  writable = no 
  guest ok = yes
  guest only = yes" | sudo tee -a /etc/samba/smb.conf
